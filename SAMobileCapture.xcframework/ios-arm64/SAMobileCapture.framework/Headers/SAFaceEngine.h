//
//  SAFaceEngine.h
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 9.11.2022.
//  Copyright © 2022 Sodec Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "SAFaceResult.h"


@interface SAFaceEngine : NSObject

- (NSMutableArray *)getFaceResultsFromImage:(UIImage *)image withResizeFaces:(BOOL)resizeFaces witGetPose:(BOOL)getPose withGetQuality:(BOOL)getQuality withGetLiveness:(BOOL)getLiveness withNormalizeLivenessResult:(BOOL)normalizeLivenessResult withGetOcclusion:(BOOL)getOcclusion;
- (void)clearEngine;
- (SAFaceResult*)getFaceResult:(NSArray*)faceResults;
- (UIImage *)getImageFromBuffer:(CMSampleBufferRef)sampleBuffer useLowerMemory:(BOOL)useLowerMemory;
- (id)initWithPath:(NSString *)modelPath;
@end
