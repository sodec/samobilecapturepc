//
//  FaceResults.h
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 10.11.2022.
//  Copyright © 2022 Sodec Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SAFaceResult : NSObject

@property (nonatomic) int left;
@property (nonatomic) int top;
@property (nonatomic) int right;
@property (nonatomic) int bottom;
@property (nonatomic) float yaw;
@property (nonatomic) float pitch;
@property (nonatomic) float roll;
@property (nonatomic) BOOL isGenuine;
@property (nonatomic) float livenessScore;
@property (nonatomic) float livenessScoreForSecurityModel;
@property (nonatomic) float livenessScoreForInitialModel;
@property (nonatomic) float livenessScoreForGrayModel;
@property (nonatomic) float mask;
@property (nonatomic) float sunglass;
@property (nonatomic) float quality;

@end
