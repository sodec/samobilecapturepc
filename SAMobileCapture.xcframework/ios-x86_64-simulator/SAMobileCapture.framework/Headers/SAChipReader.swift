/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

@objc public enum SALogLevel : Int, CaseIterable
{
    case Verbose = 0
    case Debug = 1
    case Info = 2
    case Warning = 3
    case Error = 4
    case Disabled = 5
}

@objc public enum SAChipReaderError : Int
{
    case LibraryNotLoaded
    case InvalidDateInput
    case NfcNotSupported
    case InvalidNfcTag
    case MoreThanOneNfcTagFound
    case InvalidMrzData
    case ConnectionFailed
    case ConnectionLost
    case General
}
@objc public enum SAChipReaderErrorX : Int
{
    case LIBRARY_NOT_LOADED
    case NFC_NOT_FOUND
    case UNDESIRED_NFC_TAG
    case INVALID_COUNTRY_SIGN_CERTIFICATE
    case ACCESS_DENIED
    case BAC_DENIED
    case PASSWORD_AUTHENTICATED_CNX_EST
    case CARD_SERVICE
    case GENERAL
}
@objc public enum SAChipReaderProgressStep : Int
{
    case AccessingNfcChip
    case ReadingPersonalData
    case ReadingImages
    case CheckingValidity
}

@objc public enum SAProgressBarType : Int
{
    case Long
    case Short
}

@objc public enum SADocumentCode : Int
{
    case Identity
    case Passport
}

@objc public protocol SAChipReaderDelegate
{
    func chipReaderDidCancel()
    func chipReaderDidError(chipReaderError: SAChipReaderError)
    func chipReaderDidDone(chipReaderResult: SAChipReaderResult)
    
    @objc optional func chipReaderMessageOnStart() -> NSString
    @objc optional func chipReaderMessageOnProgress(progressStep: SAChipReaderProgressStep, progressRate: Int) -> NSString
    @objc optional func chipReaderMessageOnError(chipReaderError: SAChipReaderError) -> NSString
    @objc optional func chipReaderMessageOnSuccess() -> NSString
}

@available(iOS 13, *)
@objcMembers public class SAChipReader : NSObject
{
    private var countrySignCertificateURL: URL?
    private var useOpenSSLForCMSVerification: Bool = false
    private var skipDocumentSignFailure: Bool = false
    private var documentCode: SADocumentCode = .Identity
    
    private var chipReaderDelegate: SAChipReaderDelegate?
    private let passportReader = PassportReader()
    private let passportUtils = PassportUtils()

    @objc public init(countrySignCertificateURL: URL? = nil, logLevel: SALogLevel = .Error)
    {
        super.init()
        
        self.countrySignCertificateURL = countrySignCertificateURL
        switch logLevel
        {
            case .Verbose:
                Log.logLevel = .verbose
            case .Debug:
                Log.logLevel = .debug
            case .Info:
                Log.logLevel = .info
            case .Warning:
                Log.logLevel = .warning
            case .Error:
                Log.logLevel = .error
            case .Disabled:
                Log.logLevel = .disabled
            default:
                Log.logLevel = .error
        }
        Log.storeLogs = false
        Log.clearStoredLogs()
    }
    
    @objc public func useOpenSSLForPassiveAuthentication()
    {
        self.useOpenSSLForCMSVerification = true
    }
    
    @objc public func skipDocumentSignOnFailure()
    {
        self.skipDocumentSignFailure = true
    }
    
    @objc public func setDocumentCode(_ documentCode: SADocumentCode)
    {
        self.documentCode = documentCode
    }
    
    @objc public func getProgressBarAsString(progressRate: Int, loadMark: NSString = "🔵", blankMark: NSString = "⚪️", progressBarType: SAProgressBarType = .Long) -> NSString
    {
        let countOfLoadedItems = (progressRate / (progressBarType == .Short ? 20 : 10))
        let loadedItems = String(repeating: ((loadMark as String) + " "), count: countOfLoadedItems)
        let notLoadedItems = String(repeating: ((blankMark as String) + " "), count: ((progressBarType == .Short ? 5 : 10) - countOfLoadedItems))
        return "\(loadedItems)\(notLoadedItems)" as NSString
    }
    
    @objc public func startNfcReading(chipReaderDelegate: SAChipReaderDelegate, documentNumber: String, dateOfBirth: SADate, dateOfExpiry: SADate)
    {
        self.chipReaderDelegate = chipReaderDelegate
        
        if !SALibrarySwift().isLibraryLoaded()
        {
            self.chipReaderDelegate?.chipReaderDidError(chipReaderError: SAChipReaderError.LibraryNotLoaded)
            return
        }
        
        if !isDateValid(date: dateOfBirth) || !isDateValid(date: dateOfExpiry)
        {
            self.chipReaderDelegate?.chipReaderDidError(chipReaderError: SAChipReaderError.InvalidDateInput)
            return
        }
        
        let documentNumberString: String = "\(documentNumber)"
        let dateOfBirthString: String = convertDateToString(date: dateOfBirth)
        let dateOfExpiryString: String = convertDateToString(date: dateOfExpiry)
        let mrzKey = passportUtils.getMRZKey(passportNumber: documentNumberString, dateOfBirth: dateOfBirthString, dateOfExpiry: dateOfExpiryString)
        
        var countrySignCertificateSet: Bool = false
        if let countrySignCertificateURL = self.countrySignCertificateURL
        {
            countrySignCertificateSet = true
            passportReader.setMasterListURL(countrySignCertificateURL)
        }
        passportReader.passiveAuthenticationUsesOpenSSL = useOpenSSLForCMSVerification
        
        passportReader.readPassport(mrzKey: mrzKey, customDisplayMessage:
        { (displayMessage) in
            switch displayMessage
            {
                case .requestPresentPassport:
                    if let chipReaderMessageOnStart = self.chipReaderDelegate?.chipReaderMessageOnStart
                    {
                        return chipReaderMessageOnStart() as String
                    }
                    else
                    {
                        if self.documentCode == .Identity
                        {
                            return "T.C. Kimlik Kartınızın arka yüzünü telefonunuzun arka üst kısmına temas ettiriniz. NFC ile okuma sırasında kimliğinizi telefondan uzaklaştırmayınız."
                        }
                        else
                        {
                            return "Pasaportunuzun ön kapağını telefonunuzun arka üst kısmına temas ettiriniz. NFC ile okuma sırasında pasaportunuzu telefondan uzaklaştırmayınız."
                        }
                    }
                case .authenticatingWithPassport(let progressRate):
                    if let chipReaderMessageOnProgress = self.chipReaderDelegate?.chipReaderMessageOnProgress
                    {
                        return chipReaderMessageOnProgress(.AccessingNfcChip, progressRate) as String
                    }
                    else
                    {
                        let progressBarAsString = self.getProgressBarAsString(progressRate: progressRate) as String
                        return "Çipe erişim sağlanıyor...\n\n\(progressBarAsString)"
                    }
                case .readingDataGroupProgress(let dataGroupId, let progressRate):
                    var chipReaderStep: SAChipReaderProgressStep = .CheckingValidity
                    if dataGroupId == DataGroupId.DG1 || dataGroupId == DataGroupId.DG11 || dataGroupId == DataGroupId.DG12
                    {
                        chipReaderStep = .ReadingPersonalData
                    }
                    else if dataGroupId == DataGroupId.DG2 || dataGroupId == DataGroupId.DG7
                    {
                        chipReaderStep = .ReadingImages
                    }
                
                    if let chipReaderMessageOnProgress = self.chipReaderDelegate?.chipReaderMessageOnProgress
                    {
                        return chipReaderMessageOnProgress(chipReaderStep, progressRate) as String
                    }
                    else
                    {
                        let progressBarAsString = self.getProgressBarAsString(progressRate: progressRate) as String
                        if chipReaderStep == .ReadingPersonalData
                        {
                            return "Kişisel veriler okunuyor...\n\n\(progressBarAsString)"
                        }
                        else if chipReaderStep == .ReadingImages
                        {
                            return "Resimler alınıyor...\n\n\(progressBarAsString)"
                        }
                        else
                        {
                            return "Güvenlik öğeleri kontrol ediliyor...\n\n\(progressBarAsString)"
                        }
                    }
                case .error(let passportReaderError):
                    let chipReaderError: SAChipReaderError = self.getChipReaderError(passportReaderError: passportReaderError)
                    if let chipReaderMessageOnError = self.chipReaderDelegate?.chipReaderMessageOnError
                    {
                        return chipReaderMessageOnError(chipReaderError) as String
                    }
                    else
                    {
                        switch chipReaderError
                        {
                            case .NfcNotSupported:
                                return "Cihazınızda NFC özelliği bulunmamaktadır. Lütfen NFC özelliği bulunan bir cihaz ile işleminizi gerçekleştiriniz."
                            case .InvalidNfcTag:
                                return "Geçersiz NFC etiketi algılandı. Lütfen sizden istenilen kimliği okutunuz."
                            case .MoreThanOneNfcTagFound:
                                return "Birden fazla NFC etiketi algılandı. Lütfen sadece sizden istenilen kimliği okutunuz."
                            case .InvalidMrzData:
                                return "Çipe erişim sağlanamamıştır. Lütfen doğru kimliği okuttuğunuzdan ve kişisel bilgilerinizin doğru olduğundan emin olunuz."
                            case .ConnectionFailed:
                                return "Çipe erişim sağlanamamıştır. Lütfen kimliğinizi tekrar okutunuz."
                            case .ConnectionLost:
                                return "Çipe erişim sağlanamamıştır. Lütfen okuma işlemi sırasında kimliğinizi hareket ettirmeyiniz."
                            default:
                                return "Çipe erişim hatası alınmıştır. Lütfen kimliğinizi tekrar okutunuz."
                        }
                    }
                case .successfulRead:
                    if let chipReaderMessageOnSuccess = self.chipReaderDelegate?.chipReaderMessageOnSuccess
                    {
                        return chipReaderMessageOnSuccess() as String
                    }
                    else
                    {
                        return "Çipe erişim sağlanmıştır."
                    }
            }
        }, completed: {(passportModel, passportReaderError) in
            if let passportModel = passportModel
            {
                var paceFeatureStatus : SAFeatureStatus = .NotPresent
                var paceResultStatus : SAResultStatus = .NotPresent
                if passportModel.isPACESupported
                {
                    paceFeatureStatus = .Present
                    switch(passportModel.PACEStatus)
                    {
                        case .notDone:
                            paceResultStatus = .NotChecked
                        case .failed:
                            paceResultStatus = .Failed
                        case .success:
                            paceResultStatus = .Succeeded
                    }
                }
                let paceStatus = SAStatus(feature: paceFeatureStatus, result: paceResultStatus)
                
                var bacFeatureStatus : SAFeatureStatus = .Unknown
                var bacResultStatus : SAResultStatus = .Unknown
                if paceResultStatus != .Succeeded
                {
                    bacFeatureStatus = .Present;
                    switch(passportModel.BACStatus)
                    {
                        case .notDone:
                            bacResultStatus = .NotChecked
                        case .failed:
                            bacResultStatus = .Failed
                        case .success:
                            bacResultStatus = .Succeeded
                    }
                }
                let bacStatus = SAStatus(feature: bacFeatureStatus, result: bacResultStatus)
                
                let eacFeatureStatus : SAFeatureStatus = .Unknown
                let eacResultStatus : SAResultStatus = .Unknown
                let eacStatus = SAStatus(feature: eacFeatureStatus, result: eacResultStatus)
                
                var activeAuthenticationFeatureStatus : SAFeatureStatus = .NotPresent
                var activeAuthenticationResultStatus : SAResultStatus = .NotPresent
                if passportModel.activeAuthenticationSupported
                {
                    activeAuthenticationFeatureStatus = .Present
                    if passportModel.activeAuthenticationPassed
                    {
                        activeAuthenticationResultStatus = .Succeeded
                    }
                    else
                    {
                        activeAuthenticationResultStatus = .Failed
                    }
                }
                let activeAuthenticationStatus = SAStatus(feature: activeAuthenticationFeatureStatus, result: activeAuthenticationResultStatus)
                
                var chipAuthenticationFeatureStatus : SAFeatureStatus = .NotPresent
                var chipAuthenticationResultStatus : SAResultStatus = .NotPresent
                if passportModel.isChipAuthenticationSupported
                {
                    chipAuthenticationFeatureStatus = .Present
                    switch(passportModel.chipAuthenticationStatus)
                    {
                        case .notDone:
                            chipAuthenticationResultStatus = .NotChecked
                        case .failed:
                            chipAuthenticationResultStatus = .Failed
                        case .success:
                            chipAuthenticationResultStatus = .Succeeded
                    }
                }
                let chipAuthenticationStatus = SAStatus(feature: chipAuthenticationFeatureStatus, result: chipAuthenticationResultStatus)
                
                let documentSignFeatureStatus : SAFeatureStatus = .Present
                let documentSignResultStatus : SAResultStatus = passportModel.documentSigningCertificateVerified ? .Succeeded : .Failed
                let documentSignStatus = SAStatus(feature: documentSignFeatureStatus, result: documentSignResultStatus)
                
                var countrySignFeatureStatus : SAFeatureStatus = .NotPresent
                var countrySignResultStatus : SAResultStatus = .NotPresent
                if countrySignCertificateSet
                {
                    countrySignFeatureStatus = .Present
                    countrySignResultStatus = passportModel.passportCorrectlySigned ? .Succeeded : .Failed
                }
                let countrySignStatus = SAStatus(feature: countrySignFeatureStatus, result: countrySignResultStatus)
                
                let hashesMatchFeatureStatus : SAFeatureStatus = .Present
                let hashesMatchResultStatus : SAResultStatus = passportModel.passportDataNotTampered ? .Succeeded : .Failed
                let hashesMatchStatus = SAStatus(feature: hashesMatchFeatureStatus, result: hashesMatchResultStatus)
                
                var cloningStatus: SACloningStatus = .Unknown
                if activeAuthenticationFeatureStatus == .Present
                {
                    if activeAuthenticationResultStatus == .Succeeded
                    {
                        cloningStatus = .NotCloned
                    }
                    else if activeAuthenticationResultStatus == .Failed
                    {
                        cloningStatus = .Cloned
                    }
                }
                if chipAuthenticationFeatureStatus == .Present
                {
                    if chipAuthenticationResultStatus == .Succeeded
                    {
                        if cloningStatus != .Cloned
                        {
                            cloningStatus = .NotCloned
                        }
                    }
                    else if chipAuthenticationResultStatus == .Failed
                    {
                        cloningStatus = .Cloned
                    }
                }
                
                var authenticityStatus: SAAuthenticityStatus = .Unknown
                if documentSignFeatureStatus == .Present
                {
                    if documentSignResultStatus == .Succeeded
                    {
                        authenticityStatus = .Authenticated
                    }
                    else if documentSignResultStatus == .Failed
                    {
                        if !self.skipDocumentSignFailure
                        {
                            authenticityStatus = .NotAuthenticated
                        }
                    }
                }
                if countrySignFeatureStatus == .Present
                {
                    if countrySignResultStatus == .Succeeded
                    {
                        if authenticityStatus != .NotAuthenticated
                        {
                            authenticityStatus = .Authenticated
                        }
                    }
                    else if countrySignResultStatus == .Failed
                    {
                        authenticityStatus = .NotAuthenticated
                    }
                }
                if hashesMatchFeatureStatus == .Present
                {
                    if hashesMatchResultStatus == .Succeeded
                    {
                        if authenticityStatus != .NotAuthenticated
                        {
                            authenticityStatus = .Authenticated
                        }
                    }
                    else if hashesMatchResultStatus == .Failed
                    {
                        authenticityStatus = .NotAuthenticated
                    }
                }
            
                let chipReaderResult = SAChipReaderResult(mrzInfo: passportModel.mrzInfo, additionalPersonDetails: passportModel.additionalPersonDetails, additionalDocumentDetails: passportModel.additionalDocumentDetails, biometricFace: passportModel.biometricFaceImage, portrait: passportModel.biometricFaceImage, signature: passportModel.signatureImage, fingerprints : nil, basicAccessControlStatus: bacStatus, passwordAuthenticatedCxnEstStatus: paceStatus, extendedAccessControlStatus: eacStatus, activeAuthenticationStatus: activeAuthenticationStatus, chipAuthenticationStatus: chipAuthenticationStatus, documentSignStatus: documentSignStatus, countrySignStatus: countrySignStatus, hashesMatchStatus: hashesMatchStatus, cloningStatus: cloningStatus, authenticityStatus: authenticityStatus)
                
                print("documentNumber : ",passportModel.mrzInfo.documentNumber)
                print("dateOfBirth : ",passportModel.mrzInfo.dateOfBirth)
                print("dateOfExpiry : ",passportModel.mrzInfo.dateOfExpiry)
                print("documentCode : ",passportModel.mrzInfo.documentCode)
                print("gender : ",passportModel.mrzInfo.gender)
                
                if passportModel.mrzInfo.documentNumber.contains("?") {
                    DispatchQueue.main.async
                    {
                        self.chipReaderDelegate?.chipReaderMessageOnError?(chipReaderError: .ConnectionFailed)
                    }
                }else{
                    DispatchQueue.main.async
                    {
                        self.chipReaderDelegate?.chipReaderDidDone(chipReaderResult: chipReaderResult)
                    }
                }
                
            }
            else
            {
                if let passportReaderError = passportReaderError
                {
                    switch passportReaderError
                    {
                        case NFCPassportReaderError.UserCanceled:
                            DispatchQueue.main.async
                            {
                                self.chipReaderDelegate?.chipReaderDidCancel()
                            }
                        default:
                            let chipReaderError: SAChipReaderError = self.getChipReaderError(passportReaderError: passportReaderError)
                            DispatchQueue.main.async
                            {
                                self.chipReaderDelegate?.chipReaderDidError(chipReaderError: chipReaderError)
                            }
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        self.chipReaderDelegate?.chipReaderDidError(chipReaderError: SAChipReaderError.General)
                    }
                }
            }
        })
    }
    
    private func isDateValid(date: SADate) -> Bool
    {
        return SAString.isDayValid(date.day!) && SAString.isMonthValid(date.month!) && SAString.isYearValid(date.year!);
    }
    
    private func convertDateToString(date: SADate) -> String
    {
        let yearString = "\(date.year!)"
        var dateString = "\(yearString.suffix(2))"
        
        if date.month! < 10
        {
            dateString += "0"
        }
        let monthString = "\(date.month!)"
        dateString += monthString
        
        if date.day! < 10
        {
            dateString += "0"
        }
        let dayString = "\(date.day!)"
        dateString += dayString
        return dateString
    }
    
    private func getChipReaderError(passportReaderError: NFCPassportReaderError) -> SAChipReaderError
    {
        switch passportReaderError
        {
            case .NFCNotSupported:
                return .NfcNotSupported
            case .TagNotValid:
                return .InvalidNfcTag
            case .MoreThanOneTagFound:
                return .MoreThanOneNfcTagFound
            case .InvalidMRZKey:
                return .InvalidMrzData
            case .ConnectionError:
                return .ConnectionFailed
            case .ResponseError(let description, let sw1, let sw2):
                Log.error("Error reading the chip: \(description) - (0x\(sw1) - 0x\(sw2)")
                if description == "Security status not satisfied"
                {
                    return .InvalidMrzData
                }
                else if description == "Tag connection lost"
                {
                    return .ConnectionLost
                }
                else
                {
                    return .General
                }
            default:
                return .General
        }
    }
}
