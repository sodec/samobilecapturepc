//
//  SACaptureSelfie.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 30.05.2022.
//  Copyright © 2022 Sodec Inc. All rights reserved.
//


import UIKit
import Foundation
import AVFoundation

public class SACaptureSelfie: UIView{
    
    var view: UIView!
    var faceengine : SAFaceEngine!
    @IBOutlet weak var previewView: PreviewView!
    lazy var cameraFeedManager = CameraFeedManager(previewView: previewView, position: .front)
    lazy var sAOcrImage = SAOcrImage()
    var startProcess : Bool = false
    public var delegate : SACaptureSelfieDelegate?
    public var selfieModelPath : String = ""
    public var selfieModelCertificate : String = ""
    var saProcessTypes : SAProcessTypes = .turnYourHeadLeft
   
    
    public var sACaptcha = SACaptcha() {
        didSet {
            
            setNewParameters()
        }
    }
    
    public var maxCountOfAttemptsForLiveness = 3 {
        didSet {
            
            setNewParameters()
        }
    }
    
    public var checkMaskForEveryFace : Bool = false {
        didSet {
            
            setNewParameters()
        }
    }
    
    
    
    func setNewParameters(){
        sAOcrImage.sACaptcha = sACaptcha
        sAOcrImage.maxCountOfAttemptsForLiveness = maxCountOfAttemptsForLiveness
        sAOcrImage.checkMaskForEveryFace = checkMaskForEveryFace
        
        
    }
    
    

    @IBOutlet weak var librarryErrorLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
        
    }
      
}

extension SACaptureSelfie {
    
    
    func getDocumentsDirectory() -> URL {
        // find all possible documents directories for this user
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)

        // just send back the first one, which ought to be the only one
        return paths[0]
    }
    
    
    func xibSetup() {
        
        
        
        
        
        backgroundColor = UIColor.clear
        //bluetoothButton.isHidden = true
        
        let bundle = Bundle(for: type(of: self))
        
//        let path = bundle.path(forResource: "mnniOS", ofType:"zip")
//        print("path : ",path)
//        guard let dataSod = try? Data(contentsOf: URL(fileURLWithPath: path!)) else {return}
//        print("dataSod count : ",dataSod.count)
//
//        let url = getDocumentsDirectory().appendingPathComponent("mnniOS.zip")
//        print("dataSod url : ",url)
//
//
//        do {
//            try dataSod.write(to: url)
//            guard let loadSavedDataSod = try? Data(contentsOf: url) else {return}
//            print("loadSavedDataSod count : ",loadSavedDataSod.count)
//
//        } catch {
//                        print("face.sodec 2 error : " , error.localizedDescription)
//        }
        
//        let path = bundle.path(forResource: "sod", ofType:"mnn")
//        print("bundle path : ",path)
//        let url = getDocumentsDirectory().appendingPathComponent("sodDownloaded.mnn")
//        var urlPath = url.absoluteString
//        urlPath = urlPath.replace("file://", with: "")
//        print("absoluteString path : ",urlPath)
//        guard let loadSavedDataSod = try? Data(contentsOf: URL(fileURLWithPath: urlPath)) else {return}
//        print("loadSavedDataSod count : ",loadSavedDataSod.count)
        
        
        
        
        
        
        
        
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        view =  nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = bounds
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view]))
        if SALibrarySwift().isLibraryLoaded() {
        
     
          
            
            

            librarryErrorLabel.isHidden = true
            cameraFeedManager.delegate = self
            sAOcrImage.sACaptcha = sACaptcha
            
            sAOcrImage.delegate = self
            cameraFeedManager.checkCameraConfigurationAndStartSession()
            previewView.previewLayer.videoGravity = .resizeAspectFill
            
            
            let urlToDelete = getDocumentsDirectory().appendingPathComponent("mnniOS/")
            deleteFile(url: urlToDelete)
            
//            let urlToDelete2 = getDocumentsDirectory().appendingPathComponent("mnniOS.zip")
//            deleteFile(url: urlToDelete2)
            
//            let urlDocumentTrst = getDocumentsDirectory().appendingPathComponent("mnniOS.zip")
//            let  urlPathDocumentTrst = urlDocumentTrst.absoluteString
//            selfieModelPath = urlPathDocumentTrst.replace("file://", with: "")
            

            
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: { [self] in
//                cameraFeedManager.switchCameraPosition(dType: .builtInWideAngleCamera, position: .front)
//
//            })
        }else{
            librarryErrorLabel.isHidden = false
        }
        
        

    }
    
     func deleteFile(url: URL) {
        print ("about to delete \(url)")

        do {
            try FileManager.default.removeItem(at: url)
            print ("deleted \(url)")
        } catch {
                print("Could not remove file at url: \(url.lastPathComponent)")
        }
    }
    
}


public protocol SACaptureSelfieDelegate{
    
   // func didReceivedProcessType(saProcessTypes:SAProcessTypes)
    func captureSelfieOnFaceRecognitionOnSelfie (captureSelfieResult:SACaptureSelfieResult)
    //func didReceiveOcrError (error:SAOcrError)
    //func didReceiveSelfieError(error: SASelfieError)
    func didReceiveSelfieNotification(notification: SASelfieNotification)
}









