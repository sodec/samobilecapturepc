//
//  SAMobileCapture.h
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 22.09.2022.
//

#import <Foundation/Foundation.h>
#import "SACalculators.h"
#import "SAAsciifier.h"
#import "SAString.h"
#import "SACrypto.h"
#import "SADeasciifier.h"
#import "SAFaceResult.h"
#import "SAFaceEngine.h"
#import "RSA.h"


//! Project version number for SAMobileCapture.
FOUNDATION_EXPORT double SAMobileCaptureVersionNumber;

//! Project version string for SAMobileCapture.
FOUNDATION_EXPORT const unsigned char SAMobileCaptureVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SAMobileCapture/PublicHeader.h>


