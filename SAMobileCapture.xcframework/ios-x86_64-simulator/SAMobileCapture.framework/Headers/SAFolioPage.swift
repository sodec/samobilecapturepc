//
//  SAFolioPage.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 4.04.2022.
//

import UIKit
import Foundation
import AVFoundation

public class SAFolioPage: UIView{
    
    var view: UIView!
    lazy var sAOcrImage = SAOcrImage()
    //var adjustsFilter : AdjustsFilter?
    @IBOutlet weak var previewView: PreviewView!
    @IBOutlet weak var librarryErrorLabel: UILabel!
    lazy var cameraFeedManager = CameraFeedManager(previewView: previewView, position: .back)
    public var delegate : SAFolioPageDelegate?
    var borderLayer : CAShapeLayer?
    var borderLayerFace : CAShapeLayer?
    var releaseScan : Bool = false
    var identityScan : SAIdentityScan = .scanFront
    var showBorde : Bool = false
    public var isEncryted : Bool = true
    public var countOfLetters = 50
    public var faceDetection : Bool = true
    
    public var oviModelPath : String = ""
    public var oviModelCertificate : String = ""

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
        
    }
    

    @IBAction func startFeature(_ sender: Any) {
      
       
        

    }
    
    
      
}
private extension SAFolioPage {
    
    func xibSetup() {
        
        backgroundColor = UIColor.clear
        //bluetoothButton.isHidden = true
        
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        view =  nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = bounds
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": view]))
        
        
       // adjustsFilter = AdjustsFilter()
        
        if SALibrarySwift().isLibraryLoaded() {
            librarryErrorLabel.isHidden = true
            cameraFeedManager.delegate = self
            cameraFeedManager.checkCameraConfigurationAndStartSession()
            
            sAOcrImage.delegate = self

            sAOcrImage.countOfLetters = countOfLetters
            sAOcrImage.faceDetection = faceDetection
            sAOcrImage.isEncryted = isEncryted
            borderLayer = CAShapeLayer()
            borderLayerFace = CAShapeLayer()
            self.layer.addSublayer(borderLayer!)
            self.layer.addSublayer(borderLayerFace!)
            
            previewView.previewLayer.videoGravity = .resizeAspectFill
            
            //sAOcrImage.saFeatureMatch.adjustsFilter = AdjustsFilter()
            
        }else{
            librarryErrorLabel.isHidden = false
        }
        
        
    }
    
}


public protocol SAFolioPageDelegate{
    

    func didFinishFolio(identityData : SAIdentityData?)
    func didReceivedFolioProgress(progress:Int)
    func didReceivedFolioMessage(message:SAFolioMessages)
    func didReceiveOcrError (error:SAOcrError)
    func didReceiveLightStatus(message:SACameraLightingMessages)
    func didReceivedFolioReadingMessage(message:SAFolioReadingMessages)
    func featureMatchingResult (resultTyoe:FeatureMatchingResultsTypes,resultText:String,mainImage:UIImage,resultImage:UIImage)
}
