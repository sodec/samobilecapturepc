//
//  SAAddressScanner.swift
//  SAMobileCapture
//
//  Created by Erkan SIRIN on 30.09.2022.
//  Copyright © 2022 Sodec Inc. All rights reserved.
//

import Foundation
//import SwiftyTesseract

public class SAAddressScanner: NSObject {
    
    lazy var sAOcrImage = SAOcrImage()
    
    public override init() {
        super.init()
    }
    
    public func scanAddress(image : UIImage?,tckn:String, completion: @escaping (NSError?,SAAddressData?) -> ())  {
        

//        let tesseract = Tesseract(language: RecognitionLanguage.turkish)
//
//        let result: Result<String, Tesseract.Error> = tesseract.performOCR(on: image!)
//
//        do {
//            let items = try result.get()
//            let itemsArray = items.components(separatedBy: "\n")
//            print("result itemsArray: ",itemsArray)
//
//            for item in itemsArray {
//                print("result item: ",item)
//            }
//
//        }catch let error
//            {
//                print("result error: ",error)
//            }
    
            
        
        sAOcrImage.scanAddress(image: image,tckn:tckn) { error, addressData in
            var addressWithDataImageData = SAAddressData()
            if let addressData = addressData {
                addressWithDataImageData = addressData
                addressWithDataImageData.inputImageData = image?.jpegData(compressionQuality: 1.0)
                completion(error,addressWithDataImageData)
            }else{
                completion(error,addressData)
            }
            
        }
    }
}
