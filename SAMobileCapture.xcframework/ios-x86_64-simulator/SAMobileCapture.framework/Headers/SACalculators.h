/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */


#import <UIKit/UIKit.h>
@import CoreImage;

@interface SACalculators : NSObject

- (CIRectangleFeature *)_biggestRectangleInRectangles:(NSArray *)rectangles;
- (CIRectangleFeature *)biggestRectangleInRectangles:(NSArray *)rectangles;
- (CIRectangleFeature *)highAccuracyRectangleDetectorX:(UIImage *)_clonedImage;
- (CIDetector *)highAccuracyRectangleDetector;
- (CGFloat) pointPairToBearingDegrees:(CGPoint)startingPoint secondPoint:(CGPoint) endingPoint;
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
- (NSString*)getStringFromMatch:(NSString*) valueString;
@end
