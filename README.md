# SAMobileCapture #

### 0 - Örnek proje  ###

[Örnek proje](https://bitbucket.org/sodec/samobilecapturepc/src/master/MobileCapture.zip "indir")


## 1 - Sodec Chip Reader ##

### 1a - NFC Proje ayarları  ###

- Entitlements oluşturmak için; Proje target / Signing & Capabilities / + Capability (çıkan listeden Near Field Communication Tag Reading seçilir) içeriği aşağıdaki gibi olan Projeniz.entitlements isminde bir dosya oluşturacaktır. Projeyi yayına alırken Apple sizden NDEF kımını silmenizi isteyebilir.

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>com.apple.developer.nfc.readersession.formats</key>
    <array>
        <string>NDEF</string>
        <string>TAG</string>
    </array>
</dict>
</plist>
```

- Info.plist dosyasına aşağıdaki parametreler eklenmelidir; 
```
<key>com.apple.developer.nfc.readersession.iso7816.select-identifiers</key>
    <array>
        <string>A0000002471001</string>
    </array>
    <key>NFCReaderUsageDescription</key>
    <string>Kimlik okuma açıklaması</string>
```

- Bu repoda bulunan örnek proje içerisinde “masterList.pem” isminde bir sertifika bulunur onu projenize ekleyin. Resmi kurumlar tarafından yaratılmış bir sertifikadır kimlik chip doğrulaması için gereklidir.


### 1b - SAReadChip init edilmesi ve modülün başlatılması için gerekli kodlar aşağıdadır.  ###

- SAReadChip init edilirken beklenen MRZ anahtarı içerisinde kullanılan birthDate expiryDate alanlarının formatı kimliğin arka kısmında bulunan MRZ alanında olduğu gibi YYMMDD şeklinde olmalıdır örneğin 08 Ocak 1977 olan tarih 770108 şeklinde formatlanmalıdır ay gün tek haneli ise başına 0 eklenmelidir. SAFolio ile size dönen veriler içerisinde formatlanmış halde MRZ anahtarları bulunmaktadır. 

- Kod kullanımına örnek aşağıdaki gibidir : 
```
let masterListURL = Bundle.main.url(forResource: "masterList", withExtension: ".pem")!

let sAReadChip = SAReadChip()
             
sAReadChip.readChip(idNumber: "passportNumber", dateOfBirth: "dateOfBirth", expiryDate: "expiryDate", delegate: self, masterListURL: masterListURL)
```

- SAChipDelegate üzerinden dönen fonksiyonlar aşağıdaki gibidir : 

```
    func didReadChipDone(saChipData: SAChipData) {
    }
    
    func didReadChipDoneWithError(error: NSError) {
    }
    
    func didReadChipCancel() {
    }
```
- Opsiyonel olarak NFC sürecinde ekranda görünen mesajları set edebilirsiniz edilmediği takdirde bizim belirlediğiniz varsayılan mesajlar çıkacaktır. : 

```
            sAReadChip.nfcInfoTittle = "NFC Chip özelliği olan dökümanı cihazın üst kısmına tutunuz okuma bitene kadar çekmeyiniz."
             sAReadChip.nfcReadingTittle = "Lütfen kartınızı haraket ettirmeyin"
             sAReadChip.validateDocumentTittle = "Döküman doğrulanıyor"
             sAReadChip.invalidTagTittle = "Geçersiz tag."
             sAReadChip.multiTagTittle = "Birden fazla tag bulundu lütfen aynı anda sadece bir tag okutun."
             sAReadChip.connectionErrorTittle = "Bağlantı hatası. Lütfen tekrar deneyin."
             sAReadChip.mrzErrorTittle = "MRZ anahtar uyumsuzdur."
             sAReadChip.readingErrorTittle = "Döküman okuma hatası."
             sAReadChip.readingSuccessTittle = "Döküman okuma başarılı."
             sAReadChip.corruptedDataTittle = "Part of returned data may be corrupted"
             sAReadChip.fileInvalidatedTittle = "File invalidated."
             sAReadChip.fCIErrorTittle = "FCI not formatted according to ISO7816-4 section 5.1.5"
             sAReadChip.transmissionErrorTittle = "Secured Transmission not supported"
             sAReadChip.memoryErrorTittle = "Memory failure"
             sAReadChip.commandErrorTittle = "Invalid command"
             sAReadChip.authenticationErrorTittle = "Authentication error"
             sAReadChip.invalidDataErrorTittle = "Invalid data"
           
```


        
  
## 2 - Folio ##

- Kod kullanımına örnek proje içerisinden bakabilirsiniz : MobileCapture/Controller/MCController.swift

### 2a - Folio kullanımı ###

- Folio document modülünü başlatmak için gerekli kodlar aşağıdadır.
                   
```  
@IBOutlet weak var folioView: SAFolioPage!

    override func viewDidLoad() {
        super.viewDidLoad()

        folioView.delegate = self
        folioView.isEncryted = false
    }

func didReceivedFolioMessage(message:SAFolioMessages){
        

        
    }
    
    func didFinishFolio(identityData: SAIdentityData?) {

    SADataLoader().loadDataFromDocumentPath(fileName: identityData!.faceImage!, isEncrypted: true) { [self] data in
            let image = UIImage(data: data)
        }
    }
    
    func didReceivedFolioProgress(progress:Int){
        print("progress : ",progress)

    }
    
    func didReceivedFolioReadingMessage(message: SAFolioReadingMessages) {
    
    }
    
    func didReceiveLightStatus(message:SACameraLightingMessages)
    {
    
    }
       
```   




## 3 - Selfie ve Video ##

- Kod kullanımına örnek proje içerisinden bakabilirsiniz : SelfieView.swift

### 3a - Selfie ve Video kullanımı ###

- Selfie ve Video adımlarını boş bir UIView üzerine custom class olarak kullanabilirsiniz.

- SACaptureSelfie kullanımı:

``` 
    @IBOutlet weak var selfieView: SACaptureSelfie!
    override func viewDidLoad() {
        super.viewDidLoad()

        selfieView.delegate = self
        // Do any additional setup after loading the view.
    }


func captureSelfieOnFaceRecognitionOnSelfie(selfieData: Data?, faceData: Data?) {
  
    }
                      
    
    
    func didReceivedProcessType(saProcessTypes: SAProcessTypes) {
        
  
        
    }

``` 

## 3 - Address Scanner  ##

- Kod kullanımına örnek proje içerisinden bakabilirsiniz : MCController.swift

### 3a - SAAddressScanner kullanımı ###

- 

- SAAddressScanner kullanımı:

``` 
        let addressScanner = SAAddressScanner()
        
        func scanAddress() {
        
        let addressImage = UIImage(named: "adres.jpg")
        
                addressScanner.scanAddress(image: addressImage,tckn:"00000000000") { error, addressArray in
            print("error : ",error)
            print("addressArray : ",addressArray)
        }
        
        
    }
   

``` 
- SAAddressScanner Çıktısı:

error :  nil
addressArray :  ["000000000", "İsim Soyisim", "İSTANBUL", "KADIKOY", "SUADİYE MAH.", "Sokak SK", "Apatman APT", "9", "10"]
