Pod::Spec.new do |spec|

  spec.name = 'SAMobileCapture'
  spec.version = '1.1.61.2'
  spec.summary = 'Digital ID SDK for iOS'
  spec.description = 'With Digital ID, you can provide a fast and accessible solution for Digital Authentication and KYC processes, protecting your company and your customers from fraud.'

  spec.homepage = 'https://sodec.com'
  spec.license = 'Proprietary'
  spec.authors = {'Erkan Sirin' => 'erkan.sirin@sodec.com', 'Hasan Dertli' => 'hasan.dertli@sodec.com'}

  spec.platform = :ios
  spec.ios.deployment_target = '10.0'

  spec.source = { :git => 'https://bitbucket.org/sodec/samobilecapturepc.git', :tag => '#{spec.version}' }

  spec.swift_version = '5.0'

  spec.source_files = 'SAMobileCapture.xcframework/ios-arm64/SAMobileCapture.framework/Headers/*.h'
  spec.public_header_files = 'SAMobileCapture.xcframework/ios-arm64/SAMobileCapture.framework/Headers/*.h'
  spec.vendored_frameworks = 'SAMobileCapture.xcframework'
  
  spec.ios.frameworks = ['AVFoundation', 'AudioToolbox', 'Accelerate', 'CoreGraphics', 'CoreImage', 'CoreMedia', 'CoreVideo', 'CoreText', 'CoreNFC', 'CryptoKit', 'CryptoTokenKit', 'Foundation', 'MessageUI', 'MobileCoreServices', 'OpenGLES', 'QuartzCore', 'Photos', 'ReplayKit', 'UIKit', 'Security', 'SystemConfiguration']  
  
  spec.static_framework = true
  spec.dependency 'GoogleMLKit/TextRecognition'
  spec.dependency 'GoogleMLKit/FaceDetection'
  spec.dependency 'OpenSSL-Universal','1.1.1100'
  spec.dependency 'TensorFlowLiteSwift'

  
  spec.xcconfig = { 'OTHER_LDFLAGS' => '-weak_framework CoreNFC -weak_framework CryptoKit -weak_framework CryptoTokenKit' }
  spec.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '-ObjC -l\"c++\" -l\"sqlite3\" -l\"z\" -framework AVFoundation -framework AudioToolbox -framework Accelerate -framework AssetsLibrary -framework CoreGraphics -framework CoreImage -framework CoreMedia -framework CoreVideo -framework CoreText -framework CoreTelephony -weak_framework CoreNFC -weak_framework CryptoKit -weak_framework CryptoTokenKit -framework FBLPromises -framework Foundation -framework GTMSessionFetcher -framework GoogleDataTransport -framework GoogleToolboxForMac -framework GoogleUtilities -framework GoogleUtilitiesComponents -framework LocalAuthentication -framework MLImage -framework MLKitCommon -framework MLKitFaceDetection -framework MLKitTextRecognition -framework MLKitVision -framework OpenSSL -framework nanopb -framework opencv2 -framework MessageUI -framework MobileCoreServices -framework OpenGLES -framework QuartzCore -framework Photos -framework ReplayKit -framework UIKit -framework Security -framework SystemConfiguration' }

end